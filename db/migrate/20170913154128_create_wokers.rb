class CreateWokers < ActiveRecord::Migration[5.1]
  def change
    create_table :wokers do |t|
      t.string :name
      t.string :surname
      t.string :born
      t.integer :gender
      t.string :position
      t.references :department, index: true, foreign_key: true, null: false

      t.timestamps
    end
  end
end
