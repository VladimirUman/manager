# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Woker.delete_all
Department.delete_all
Department.create(id: 1, title: 'Отдел продаж', description: 'поиск клиентов')
Department.create(id: 2, title: 'Отдел реклами', description: 'размещение реклами')
Woker.create!(name: 'Вася', surname: 'Куролесов', gender: 1, born: '01.01.1990', position: 'маркетолог', department_id: 1)
Woker.create!(name: 'Коля', surname: 'Пупкин', gender: 1, born: '02.03.1990', position: 'маркетолог', department_id: 1)
Woker.create!(name: 'Лена', surname: 'Иванова', gender: 2, born: '01.01.1992', position: 'уборщица', department_id: 1)
Woker.create!(name: 'Света', surname: 'Курочкина', gender: 2, born: '02.03.1993', position: 'менеджер', department_id: 2)
Woker.create!(name: 'Иван', surname: 'Дорн', gender: 1, born: '01.01.1991', position: 'маркетолог', department_id: 2)
Woker.create!(name: 'Саша', surname: 'Пупкин', gender: 1, born: '02.03.1990', position: 'маркетолог', department_id: 2)
Woker.create!(name: 'Анна', surname: 'Курочкина', gender: 2, born: '02.03.1993', position: 'менеджер', department_id: 2)
Woker.create!(name: 'Григорий', surname: 'Дорн', gender: 1, born: '01.01.1991', position: 'маркетолог', department_id: 2)
Woker.create!(name: 'Петя', surname: 'Пупкин', gender: 1, born: '02.03.1990', position: 'маркетолог', department_id: 2)
Woker.create!(name: 'Алена', surname: 'Курочкина', gender: 2, born: '02.03.1993', position: 'менеджер', department_id: 2)
Woker.create!(name: 'Василий', surname: 'Дорн', gender: 1, born: '01.01.1991', position: 'маркетолог', department_id: 2)
Woker.create!(name: 'Олежка', surname: 'Ляшко', gender: 1, born: '02.03.1990', position: 'маркетолог', department_id: 2)
Woker.create!(name: 'Света', surname: 'Звезда', gender: 2, born: '02.03.1993', position: 'менеджер', department_id: 2)
Woker.create!(name: 'Иван', surname: 'Иванов', gender: 1, born: '01.01.1991', position: 'маркетолог', department_id: 2)
Woker.create!(name: 'Саша', surname: 'Малаша', gender: 1, born: '02.03.1990', position: 'маркетолог', department_id: 2)
