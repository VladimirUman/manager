require 'rails_helper'

RSpec.describe "Wokers", type: :request do


  let(:user) { { email: "user3@m.com", password: "123456789" } }

  before do
    sign_in create(:user)
  end

  let!(:department) { create(:department) }
  let!(:wokers) { create_list(:woker, 20, department_id: department.id) }
  let(:department_id) { department.id }
  let(:id) { wokers.first.id }

  # Test suite for GET /departments/:department_id/wokers
  describe 'GET api/v1/departments/:department_id' do
    before { get "/api/v1/departments/#{department_id}" }

    context 'when department exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all department wokers' do
        expect(json.size).to eq(20)
      end
    end

    context 'when department does not exist' do
      let(:department_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match("{\"errors\":\"Not found\"}")
      end
    end
  end

  # Test suite for GET /wokers/:id
  describe 'GET api/v1/wokers/:id' do
    before { get "/api/v1/wokers/#{id}" }

    context 'when department woker exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the woker' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when department woker does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match("{\"errors\":\"Not found\"}")
      end
    end
  end

  # Test suite for PUT /departments/:department_id/wokers
  describe 'POST api/v1/departments/:department_id/wokers' do
    let(:valid_attributes) { { name: 'Valodya', surname: 'Ulyanov', gender: 'Мужчина', born: '20.08.1981', position: 'boss' } }
    let(:invalid_attributes) { { name: 'Valodya', gender: 'Мужчина', born: '20.08.1981', position: 'boss' } }

    context 'when request attributes are valid' do
      before { post "/api/v1/departments/#{department_id}/wokers", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/api/v1/departments/#{department_id}/wokers", params: invalid_attributes }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(json['surname']).to match(["can't be blank"])
      end
    end
  end

  # Test suite for PUT /wokers/:id
  describe 'PUT api/v1/wokers/:id' do
    let(:valid_attributes) { { name: 'Mozart' } }

    before { put "/api/v1/wokers/#{id}", params: valid_attributes }

    context 'when woker exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'updates the woker' do
        updated_woker = Woker.find(id)
        expect(updated_woker.name).to match(/Mozart/)
      end
    end

    context 'when the woker does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(json['errors']).to match("Not found")
      end
    end
  end

  # Test suite for DELETE /wokers/:id
  describe 'DELETE api/v1/wokers/:id' do
    before { delete "/api/v1/wokers/#{id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

end
