require 'rails_helper'

RSpec.describe 'Departments', type: :request do
  # initialize test data
  let!(:departments) { create_list(:department, 15) }
  let(:department_id) { departments.first.id }
  let(:user) { { email: "user3@m.com", password: "123456789" } }

  before do
    sign_in create(:user)
  end

  # Test suite for GET /departments
  describe 'GET api/v1/departments' do
    # make HTTP get request before each example
    before { get '/api/v1/departments' }

    it 'returns departments' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /departments/:id
  describe 'GET api/v1/departments/:id' do
    before { get "/api/v1/departments/#{department_id}" }

    context 'when the record exists' do
      it 'returns the department' do
        expect(response.body).not_to be_empty
        #expect(json['id']).to eq(department_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:department_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found department' do
        expect(json['errors']).to match("Not found")
      end
    end
  end

  # Test suite for POST /departments
  describe 'POST api/v1/departments' do
    # valid payload
    let(:valid_attributes) { { title: "Marketing", description: "marketing" } }

    context 'when the request is valid' do
      before { post '/api/v1/departments', params: valid_attributes }

      it 'creates a department' do
        expect(json["title"]).to eq("Marketing")
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/departments', params: { title: 'Foobar' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(json["description"]).to match(["can't be blank"])
      end
    end
  end

  # Test suite for PUT /departments/:id
  describe 'PUT api/v1/departments/:id' do
    let(:valid_attributes) { { title: "Planing depatment" } }

    context 'when the record exists' do
      before { put "/api/v1/departments/#{department_id}", params: valid_attributes }

      it 'updates the record' do
        updated_department = Department.find(department_id)
        expect(updated_department.title).to match(/Planing depatment/)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  # Test suite for DELETE /departments/:id
  describe 'DELETE api/v1/departments/:id' do
    before { delete "/api/v1/departments/#{department_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
