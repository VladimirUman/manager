require 'rails_helper'

RSpec.describe 'Users', type: :request do
  # initialize test data
  let!(:users) { create_list(:user, 5) }
  let(:user_id) { users.first.id }

  before do
    sign_in(users.first)
  end

  # Test suite for GET /users
  describe 'GET api/v1/users' do
    # make HTTP get request before each example
    before { get '/api/v1/users' }

    it 'returns users' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(5)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /users/:id
  describe 'GET api/v1/users/:id' do
    before { get "/api/v1/users/#{user_id}" }

    context 'when the record exists' do
      it 'returns the user' do
        expect(response.body).not_to be_empty
        #expect(json['id']).to eq(user_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:user_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found user' do
        expect(json['errors']).to match("Not found")
      end
    end
  end

  # Test suite for POST /users
  describe 'POST api/v1/users' do
    # valid payload
    let(:valid_attributes) { { email: "user@examle.com", password: "marketing" } }

    context 'when the request is valid' do
      before { post '/api/v1/users', params: valid_attributes }

      it 'creates a user' do
        expect(json["email"]).to eq("user@examle.com")
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/api/v1/users', params: { email: "user@examle.com" } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(json["password"]).to match(["can't be blank"])
      end
    end
  end

  # Test suite for PUT /users/:id
  describe 'PUT api/v1/users/:id' do
    let(:valid_attributes) { { email: "user_edit@examle.com" } }

    context 'when the record exists' do
      before { put "/api/v1/users/#{user_id}", params: valid_attributes }

      it 'updates the record' do
        updated_user = User.find(user_id)
        expect(updated_user.email).to match(/user_edit@examle.com/)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end

  # Test suite for DELETE /users/:id
  describe 'DELETE api/v1/users/:id' do
    before { delete "/api/v1/users/#{user_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end
