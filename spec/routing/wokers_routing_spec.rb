require "rails_helper"

RSpec.describe WokersController, type: :routing do
  describe "routing" do

    it "routes to #show" do
      expect(:get => "/wokers/1").to route_to("wokers#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/departments/1/wokers").to route_to("wokers#create", :department_id => "1")
    end

    it "routes to #update via PUT" do
      expect(:put => "/wokers/1").to route_to("wokers#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/wokers/1").to route_to("wokers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/wokers/1").to route_to("wokers#destroy", :id => "1")
    end

  end
end
