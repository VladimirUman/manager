FactoryGirl.define do
  factory :department do
    title { Faker::Name.title }
    description { Faker::Lorem.word }
  end
end
