FactoryGirl.define do
  factory :woker do
    name { Faker::Name.first_name }
    surname { Faker::Name.last_name }
    born { Faker::Date.birthday(18, 65) }
    gender { "Мужчина" }
    position { Faker::Name.title }
    department_id nil
  end
end
