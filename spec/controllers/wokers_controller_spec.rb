require 'rails_helper'

RSpec.describe WokersController, type: :controller do

  let(:user) { FactoryGirl.create(:user) }

  before do
    sign_in user
  end

  let(:department) {
    { id: 10,
      title: "Маркетінг",
    description: "Продають" }
    }
  before do
    @department = Department.create! department
  end

  # This should return the minimal set of attributes required to create a valid
  # Woker. As you add validations to Woker, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
     {
       name: "Гриша",
       surname: "Лепс",
       born: "12.09.1966",
       gender: "Мужчина",
       position: "начальник",
       department_id: @department.id
     }
  }

  let(:invalid_attributes) {
    {
      name: "Гриша",
      surname: nil,
      born: "12.09.1966",
      gender: "Мужчина",
      position: "начальник",
      department: @department.id
    }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # WokersController. Be sure to keep this updated too.
  let(:valid_session) { { email: "grisha@example.com",
  password: "123456789" } }

  describe "GET #show" do
    it "returns a success response" do
      woker = Woker.create! valid_attributes
      get :show, params: {id: woker.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Woker" do
        expect {
          post :create, params: {department_id: @department.id, woker: valid_attributes}, session: valid_session
        }.to change(Woker, :count).by(1)
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { {name: "Вася"}
      }

      it "updates the requested woker" do
        woker = Woker.create! valid_attributes
        put :update, params: {id: woker.to_param, woker: new_attributes}, session: valid_session
        woker.reload
        expect(woker.attributes).to include( { "name" => "Вася"} )
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested woker" do
      woker = Woker.create! valid_attributes
      expect {
        delete :destroy, params: {id: woker.to_param}, session: valid_session
      }.to change(Woker, :count).by(-1)
    end
  end

end
