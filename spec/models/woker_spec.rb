require 'rails_helper'

RSpec.describe Woker, type: :model do
  # Association test
  # ensure an woker record belongs to a single department record
  it { should belong_to(:department) }
  # Validation test
  # ensure columns are present before saving
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:surname) }
  it { should validate_presence_of(:gender) }
  it { should validate_presence_of(:born) }
  it { should validate_presence_of(:department) }
end
