require 'rails_helper'

RSpec.describe Department, type: :model do
  # Association test
  # ensure Department model has a 1:m relationship with the Woker model
  it { should have_many(:wokers).dependent(:destroy) }
  # Validation tests
  # ensure columns title present and unique before saving
  it { should validate_presence_of(:title) }
  it { should validate_uniqueness_of(:title) }
  it { should validate_presence_of(:description) }

end
