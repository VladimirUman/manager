class DepartmentSerializer < ActiveModel::Serializer
  attributes :id, :title, :description
end
