class WokerSerializer < ActiveModel::Serializer
  attributes :id, :name, :surname, :gender, :position, :born, :department_id
end
