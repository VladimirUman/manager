class WokersController < ApplicationController
  before_action :set_woker, only: [:show, :edit, :update, :destroy]

  # GET /wokers/1
  def show
    @department = Department.find(@woker.department_id)
  end

  # GET /wokers/new
  def new
    @department = Department.find(params[:department_id])
    @woker = @department.wokers.build
  end

  # POST /wokers
  def create
    @department = Department.find(params[:department_id])
    @woker = @department.wokers.build(woker_params)

    if @woker.save
      redirect_to department_path(@department), flash: { notice: "Woker #{@woker.surname} added!" }
    else
      redirect_to department_path(@department), flash: { notice: "Woker #{@woker.surname} not added!" }
    end
  end

  # GET /wokers/edit/1
  def edit
    @departments = Department.all.map{|c| [ c.title, c.id ] }
  end

  # PATCH/PUT /wokers/1
  def update
    @department = Department.find(@woker.department_id)
    @departments = Department.all.map{|c| [ c.title, c.id ] }
    if @woker.update(woker_params)
      redirect_to department_path(@department), flash: { notice: "Woker #{@woker.surname} updated!" }
    else
      redirect_to department_path(@department), flash: { notice: "Woker #{@woker.surname} not updated!" }
    end
  end

  # DELETE /wokers/1
  def destroy
    @department = Department.find(@woker.department_id)
    @woker.destroy
    redirect_to department_path(@department), flash: { notice: "Woker #{@woker.surname} deleted!" } 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_woker
      @woker = Woker.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def woker_params
      params.fetch(:woker, {}).permit(:name, :surname, :position, :department_id, :born, :gender)
    end
end
