class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  protect_from_forgery with: :null_session, unless: -> { request.format.json? }
  respond_to :json, :html

  private
    def authorize_admin
      unless current_user.admin?
        respond_to do |format|
          format.html { redirect_to root_path, flash: { notice: 'Access Denied' } }
          format.json { render json: { errors: "Access denied" }, status: 423 }
        end
      end
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
      render json: { errors: "Not found" }, status: 404
    end

end
