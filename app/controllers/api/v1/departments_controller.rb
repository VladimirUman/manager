module Api
  module V1
    class DepartmentsController < ApplicationController
      before_action :set_department, only: [:show, :update, :destroy]

      # GET /departments
      def index
        if params[:sort]
          @departments = Department.order(params[:sort]).all.page(params[:page]).per(10)
        else
          @departments = Department.all.page(params[:page]).per(10)
        end
        render json: @departments
      end

      # GET /departments/1
      def show
        if params[:sort]
          @wokers = Woker.order(params[:sort]).where(department_id: @department.id).page(params[:page]).per(20)
        else
          @wokers = Woker.where(department_id: @department.id).page(params[:page]).per(20)
        end
        render json: @wokers
      end


      # POST /departments
      def create
        @department = Department.new(department_params)

        if @department.save
          render json: @department, status: :created, location: @department
        else
          render json: @department.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /departments/1
      def update
        if @department.update(department_params)
          render json: @department
        else
          render json: @department.errors, status: :unprocessable_entity
        end
      end

      # DELETE /departments/1
      def destroy
        @department.destroy
        head :no_content
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_department
          @department = Department.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def department_params
          params.except(:format, :department).permit(:title, :description)
        end

    end
  end
end
