module Api
  module V1
    class WokersController < ApplicationController
      before_action :set_woker, only: [:show, :update, :destroy]

      # GET /wokers/1
      def show
        @department = Department.find(@woker.department_id)
        render json: @woker
      end

      # POST /wokers
      def create
        @department = Department.find(params[:department_id])
        @woker = @department.wokers.build(woker_params)

        if @woker.save
          render json: @woker, status: :created, location: @woker
        else
          render json: @woker.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /wokers/1
      def update
        @department = Department.find(@woker.department_id)
        if @woker.update(woker_params)
          render json: @woker
        else
          render json: @woker.errors, status: :unprocessable_entity
        end
      end

      # DELETE /wokers/1
      def destroy
        @department = Department.find(@woker.department_id)
        @woker.destroy
        head :no_content
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_woker
          @woker = Woker.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def woker_params
          params.except(:format, :woker).permit(:name, :surname, :position, :department_id, :born, :gender)
        end
    end
  end
end
