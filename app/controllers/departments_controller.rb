class DepartmentsController < ApplicationController
  before_action :set_department, only: [:show, :edit, :update, :destroy]

  # GET /departments
  def index
    if params[:sort]
      @departments = Department.order(params[:sort]).all.page(params[:page]).per(10)
    else
      @departments = Department.all.page(params[:page]).per(10)
    end
  end

  # GET /departments/1
  def show
    if params[:sort]
      @wokers = Woker.order(params[:sort]).where(department_id: @department.id).page(params[:page]).per(20)
    else
      @wokers = Woker.where(department_id: @department.id).page(params[:page]).per(20)
    end
  end


  # GET /departments/new
  def new
    @department = Department.new
  end

  # POST /departments
  def create
    @department = Department.new(department_params)

    if @department.save
      redirect_to departments_path, flash: { notice: "Department added!" }
    else
      redirect_to departments_path, flash: { notice: "Department not added!" }
    end
  end

  # GET /departments/edit/1
  def edit

  end

  # PATCH/PUT /departments/1
  def update
    if @department.update(department_params)
      redirect_to departments_path, flash: { notice: "Department updated!" }
    else
      redirect_to departments_path, flash: { notice: "Department not updated!" }
    end
  end

  # DELETE /departments/1
  def destroy
    @department.destroy
    redirect_to departments_path, flash: { notice: "Department deleted!" }
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_department
      @department = Department.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def department_params
      params.fetch(:department, {}).permit(:title, :description)
    end

end
