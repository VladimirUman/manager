class UsersController < ApplicationController
  before_action :authorize_admin
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  def index
    @users = User.all
  end

  # GET /users/1
  def show

  end


  # GET /users/new
  def new
    @user = User.new
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to users_path, flash: { notice: "User #{@user.email} was successfully created." }
    else
      redirect_to users_path
    end
  end

  # GET /users/edit/1
  def edit

  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      redirect_to users_path, flash: { notice: "User #{@user.email} was successfully updated!" }
    else
      redirect_to users_path
    end
  end

  # DELETE /users/1
  def destroy
    if current_user == @user
      redirect_to users_path, notice: "You can't destroy yourself."
    else
      @user.destroy
      redirect_to users_path, flash: { notice: "User #{@user.email} was successfully deleted!" }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.fetch(:user, {}).permit(:email, :password, :admin)
    end
end
