class Department < ApplicationRecord
  has_many :wokers, dependent: :destroy
  validates :title, presence: true, uniqueness: true
  validates :description, presence: true
end
