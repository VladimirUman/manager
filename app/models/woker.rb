class Woker < ApplicationRecord
  enum gender: {
    "Мужчина" => 1,
    "Женщина" => 2
  }

  belongs_to :department

  validates :name, :surname, :position, :gender, :born, :department, presence: true
  validates :gender, inclusion: genders.keys

end
