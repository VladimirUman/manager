Rails.application.routes.draw do
  devise_for :users, only: [:sessions]

  root 'departments#index'
  resources :users
  resources :departments, shallow: true do
    resources :wokers, except: [:index]
  end


  namespace 'api', format: 'json' do
    namespace 'v1' do
      resources :users
      resources :departments, shallow: true do
        resources :wokers, except: [:index]
      end
    end
  end

end
