#README

Приложение по управлению людьми и департаментами компании. Состоят из 2х частей: API endpoints для мобильного приложения и несколько HTML страниц для демонстрации на
Веб. 
Админстратор может создавать, удалять/редактировать пользователей
Пользователь может аутентифицироваться
Пользователь может создать новый департамент компании (название, описание).
Пользователь может создать сотрудника (имя, год рождения, пол, должность).
Пользователь может добавлять в департамент сотрудников
Пользователь может просмотреть все созданные департаменты
Пользователь может просмотреть всех сотрудников определенного департамента (наличие пагинации)
Пользователь может сортировать список департаментов (A-Z)
Пользователь может сортировать списки сотрудников по имени (A-Z), по году рождения, полу
Пользователь может редактировать/удалять сотрудника
Пользователь может редактировать/удалять департамент

    Authentication:

      user:  
        email: user@m.ua
        password: 123456789

      admin:
        email: admin@m.ua
        password: 123456789

    Content Type :
       application/json

    API Requests:

    => Sign in
          url: http://localhost:3000/users/sign_in.json
          method: Post
         Body : { 	"user": { "email": "admin@m.ua", "password": "123456789"}    }

    => Sign_out
         url: http://localhost:3000/users/sign_out.json
         method: GET
         Body : not needed


    Departments:

    => listing Departments
       url: http://localhost:3000/api/v1/departments
       method: GET
       body : not needed
       parameters : can be "page" or/and "sort" (title)

    => Retrieving Department wokers
        url: http://localhost:3000/api/v1/departments/:id
        method: GET
        body : not needed

    => creating departments
       url: http://localhost:3000/api/v1/departments
       method: Post
       Body : {
              	"title": "New department", "description": "Some department"
              }

    => Updating Department
        url: http://localhost:3000/api/v1/departments/:id
        method: PUT
        Body : {
                 "title": "New department", "description": "Best department"
               }

    => Deleting Department
        url: http://localhost:3000/api/v1/departments/:id
        method: DELETE
        body : not needed

    Wokers:

    => listing Wokers by Department
       url: http://localhost:3000/api/v1/departments/:id
       method: GET
       body : not needed
       parameters : can be "page" or/and "sort" (name, surname, born, gender)

    => Retrieving Woker detail
        url: http://localhost:3000/api/v1/wokers/:id
        method: GET
        body : not needed

    => creating Wokers
       url: http://localhost:3000/api/v1/departments/:id/wokers
       method: Post
       Body : {
                  "name": "Ivan",
                  "surname": "Ivanov",
                  "gender": "Мужчина",
                  "position": "manager",
                  "born": "20.09.1980"
              }

    => Updating Woker
        url: http://localhost:3000/api/v1/wokers/:id
        method: PUT
        Body : {
                 "name": "Ivan",
                 "surname": "Ivanovich",
                 "gender": "Мужчина",
                 "position": "boss",
                 "born": "20.09.1980"
             }

    => Deleting Woker
        url: http://localhost:3000/api/v1/wokers/:id
        method: DELETE
        body : not needed

      Users:

      => listing users (only for admin)
         url: http://localhost:3000/api/v1/users
         method: GET
         body : not needed

      => Retrieving User detail (only for admin)
          url: http://localhost:3000/api/v1/users/:id
          method: GET
          body : not needed

      => creating users (only for admin)
         url: http://localhost:3000/api/v1/users
         method: Post
         Body : { "email" : "user2@example.com",
              "password" : "123456789",
              "admin" : "false"  }

      => Updating User (only for admin)
        url: http://localhost:3000/api/v1/users/:id
        method: PUT
        Body : { "email" : "user3@example.com",
                "password" : "123456789",
                "admin" : "false" }

      => Deleting User (only for admin)
        url: http://localhost:3000/api/v1/users/:id
        method: DELETE
        body : not needed
    